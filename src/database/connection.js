const mongoose = require('mongoose');

//const MONGO_URL_1 = "mongodb+srv://devteste:devteste@items-correction-db-xyxbv.mongodb.net/test?retryWrites=true&w=majority";

const MONGO_URL = "mongodb://devteste:devteste@items-correction-db-shard-00-00-xyxbv.mongodb.net:27017,items-correction-db-shard-00-01-xyxbv.mongodb.net:27017,items-correction-db-shard-00-02-xyxbv.mongodb.net:27017/test?ssl=true&replicaSet=items-correction-db-shard-0&authSource=admin&retryWrites=true&w=majority"

const connectDB = async () => {
    await mongoose.connect(MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})
    .then(function () { 
        console.log("MongoDB conectado!!!")
    })
    .catch(error => console.log(error));
}


module.exports = connectDB;