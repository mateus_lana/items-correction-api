const Item = require('../models/Item');
const Chave = require('../models/Chave');
const Reservada = require('../models/Reservada');

module.exports = {
    
    async itens(request, response){
        try {
            const itens = await Item.find().populate('chave');
            return response.send({itens});
        } catch (error) {
            return response.status(400).send({error: 'Erro ao carregar itens'})
        }
    },

    async criarItem(request, response){
        try {
            const { id, item, referencia, sequencial, solicitacao, situacao, ordem, chave } = request.body;
            const novoItem = await Item.create({id, item, referencia, sequencial, solicitacao, situacao, ordem});

            await Promise.all(chave.map(async c => {
                const itemChave = new Chave({...c, item: novoItem._id});
                await itemChave.save();
                novoItem.chave.push(itemChave);
            }));

            await novoItem.save();

            return response.send({novoItem});
        } catch (error) {
            console.log(error)
            return response.status(400).send({error: 'Erro ao criar novo item'})
        }
    },

    async proximoItem(request, response){
        try {
            const reservada = request.query.reservada;

            if(!reservada){
                const itens = await Item.find();
                let menorOrdem = Number.MAX_SAFE_INTEGER; 
                itens.map(item =>{
                    if(item.situacao === "DISPONIVEL"){
                        if(item.ordem < menorOrdem){
                            menorOrdem = item.ordem;
                        }
                    }
                })
                if(menorOrdem < Number.MAX_SAFE_INTEGER){
                    const item = await Item.findOne({ordem: menorOrdem}).populate('chave');
                    return response.send({data: item, situacao: "SUCESSO"});
                }
                return response.status(400).json({data: null, situacao: "ERRO", tipo: "SEM_CORRECAO", descricao:"Nao existe mais correcoes"})

            }
            // else{
            //     correcao = await Reservada.find();
                
            //     correcao.reservadas.map(item =>{
            //         let menorOrdem = item.ordem;
            //         if(item.ordem < menorOrdem){
            //             menorOrdem = item.ordem;
            //         }
            //     })
            //     const item = await Reservada.reservadas.find({'ordem':{menorOrdem}});
            //     return response.send({data: item, situacao: "SUCESSO"});
            // }

            
        } catch (error) {
            return response.status(400).json({data: null, situacao: "ERRO", tipo: "SEM_CORRECAO", descricao:"Nao existe mais correcoes"})
        }
    },

    async salvarItem(request, response){
        try{
            const { chave } = request.body;
            let encontrado = false;
            var valorOpcao;

            const itemParaCorrigir = await Item.findOne({id: request.params.idCorrecao}).populate('chave');

            chave.map(c => {
                itemParaCorrigir.chave.map(itemChave =>{
                    if(itemChave.id === c.id){
                        encontrado = true;
                        valorOpcao = c.valor;
                        idOpcao = c.id;
                        return;
                    }
                })
            })
            if(!encontrado){
                return response.status(400).send({
                    situacao: "ERRO",
                    tipo: "CHAVE_INCORRETA",
                    descrição: `Chave de correção incorreta. Valor ‘${chave[0].id}’ não é válido para o item ${itemParaCorrigir.item}`
                })
            }
            else{
                if(itemParaCorrigir.situacao !== 'DISPONIVEL'){
                    return response.status(400).send({
                        situacao: "ERRO",
                        tipo: "ITEM_CORRIGIDO",
                        descrição: "Item já corrigido"
                    })
                }
                // if(){
                //     return response.status(404).send({
                //         situacao: "ERRO",
                //         tipo: "ITEM_INVALIDO",
                //         descrição: "Item inválido para correção"
                //     })
                // }
                else{
                    itemParaCorrigir.chave.map(c => {
                        if(c.id == idOpcao){
                            c.opcoes.map(op =>{
                                if(valorOpcao === op.valor){
                                    itemParaCorrigir.situacao = op.descricao;
                                    return;
                                }
                            })
                        }
                    })
                    const { id, item, referencia, sequencial, solicitacao, situacao, ordem } = itemParaCorrigir;
                    const itemCorrigido = await Item.findByIdAndUpdate(itemParaCorrigir._id,{
                        id, 
                        item, 
                        referencia, 
                        sequencial, 
                        solicitacao, 
                        situacao, 
                        ordem
                    })
                    await itemCorrigido.save();
                    return response.send({
                        situacao: "SUCESSO",
                        descrição: "Correção salva com sucesso"
                    });
                }
            }
        }catch(error){
            return response.status(404).send({error: "Correcao nao existe"})
        }
    },

    async reservarItem(request, response){
        const { chave } = request.body;
        let encontrado = false;

        const itemParaReservar = await Item.findOne({id: request.params.idCorrecao}).populate('chave');

        chave.map(c => {
            itemParaReservar.chave.map(itemChave =>{
                if(itemChave.id === c.id){
                    encontrado = true;
                    return;
                }
            })
        })
        if(!encontrado){
            return response.status(404).send({
                situacao: "ERRO",
                tipo: "CHAVE_INCORRETA",
                descrição: `Chave de correção incorreta. Valor ‘${chave[0].id}’ não é válido para o item ${itemParaReservar.item}`
            })
        }
        else{
            if(itemParaReservar.situacao !== 'DISPONIVEL'){
                return response.status(404).send({
                    situacao: "ERRO",
                    tipo: "ITEM_CORRIGIDO",
                    descrição: "Item já corrigido"
                })
            }
            else{
            }
        }
    }
}