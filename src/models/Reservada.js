const mongoose = require('mongoose');

const ReservadaSchema = new mongoose.Schema({
    reservadas: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'item'
    }]
})

const Reservada = mongoose.model('reservada', ReservadaSchema);

module.exports = Reservada;

