const mongoose = require('mongoose');

const ItemSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true
    },
    item: {
        type: String,
        required: true
    },
    referencia: {
        type: String,
        required: true
    },
    sequencial: {
        type: String,
        required: true
    },
    solicitacao: {
        type: String,
        required: true
    },
    situacao: {
        type: String,
        required: true
    },
    ordem: {
        type: Number,
        required: true
    },
    // chave: [{
    //     id: Number,
    //     titulo: String,
    //     opcoes: [
    //         {
    //             valor: String,
    //             descricao: String
    //         }
    //     ]
    // }]
    chave: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'chave',
        // required: true
    }]
})

const Item = mongoose.model('item', ItemSchema);

module.exports = Item;

