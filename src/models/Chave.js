const mongoose = require('mongoose');

const ChaveSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true
    },
    titulo: {
        type: String,
        required: true
    },
    opcoes: [
        {
            valor: String,
            descricao: String
        }
    ],
    // opcoes: [{
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'opcao',
    //     required: true
    // }],
    item: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'item',
        required: true
    }
})

const Chave = mongoose.model('chave', ChaveSchema);

module.exports = Chave;

