const mongoose = require('mongoose');

const OpcaoSchema = new mongoose.Schema({
    valor: {
        type: String,
        required: true
    },
    descricao: {
        type: String,
        required: true
    },
    chave: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'chave',
        required: true
    },
    // createdAt: {
    //     type: Date,
    //     default: Date.now
    // }
})

const Opcao = mongoose.model('opcao', OpcaoSchema);

module.exports = Opcao;

