const express = require('express');
const routes = require('./routes');
const connectDB = require('./database/connection')

connectDB();
const app = express();

app.use(express.json());
app.use(routes);
app.listen(3333);