const express = require('express');

const ItemController = require('./controllers/ItemController');

const routes = express.Router();


//Create e Get para faciliar
routes.post('/itens/novo', ItemController.criarItem);
routes.get('/itens', ItemController.itens);

//Funcoes
routes.get('/correcoes/proxima', ItemController.proximoItem);
routes.post('/correcoes/:idCorrecao', ItemController.salvarItem);   
// routes.post('/correcoes/reservadas/:idCorrecao', ItemController.reservarItem);
// routes.get('/correcoes/reservadas', ItemController.itensReservados);

module.exports = routes;